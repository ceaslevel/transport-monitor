import mqtt from 'async-mqtt'
import pool from '../db/connectDB'


interface ParsedMessage {
	topic: string
	payload: Object
	transport_mode: string
	tst: string
	timestamp: number
	spd: number
	route: string
	lat: number
	long: number
	hdg: number
}

export class Subscriber {
	private broker: string
	private topic: string 
	private vh: string
	private verbose: boolean
	private timeout: number
	public messageCount: number

	constructor (broker: string, topic: string, vh: string, verbose?: boolean, timeout?: number) {
		this.broker = broker
		this.topic = topic
		this.vh = vh
		this.verbose = verbose || false
		this.timeout = timeout || 1000
		this.messageCount = 0
	}

	private async saveMessage(message, queryString) {

		try {
			const client = await pool.connect()
			const start = Date.now()
			// const sql = `SELECT NOW()`
			const result = await client.query(queryString)
			console.log(result.rows)
			const t = Date.now() - start
			this.messageCount += 1

			if (this.verbose) { 
				let messagecount = this.messageCount
				console.log('Subscriber executed query', 
					{ qs: queryString, duration: t, row_count: result.rowCount, rows: result.rows })
				console.log('@saveMessage:',{ message, messages_saved: messagecount, topic: this.topic})
			}
			client.release()

		} catch (err) {
			console.error(err)
		}
	}

	private parseMessage (topic, message) {
		// console.log(message)
		const vh = this.vh
		const msg = JSON.parse(message).VP
		const pm: ParsedMessage = {
			topic: topic,
			payload: message,
			transport_mode: vh,
			tst: msg.tst || null,
			timestamp: msg.tst != null ? new Date(msg.tst).getTime() : 0,
			// null time stamp is the same as 0 unix time fyi
			spd: msg.spd || -1,
			route: msg.route || 'MISSING',
			lat: msg.lat || 1000,
			long: msg.long || 1000,
			hdg: msg.hdg || -1,
		}

		const queryString = `
		INSERT INTO positions 
		(transport_mode, utime, spd, route, lat, long, hdg) 
		VALUES (
			'${pm.transport_mode}', 
			'${pm.timestamp}', 
			'${pm.spd}', 
			'${pm.route}', 
			'${pm.lat}', 
			'${pm.long}', 
			'${pm.hdg}'
		);`

		return { parsed: pm, query: queryString }
	}

	public async subAndSave() {

			try {
				const client = await mqtt.connectAsync(this.broker)
				if (this.verbose) console.log(`Connected to ${this.broker}: ${client.connected}`)

				const subscription = await client.subscribe(this.topic)
				if (this.verbose) console.log({ subscription: subscription })

				client.on('message', (t, msg) => {
					const message = this.parseMessage(t, msg.toString())
					this.saveMessage(message.parsed.payload, message.query)
				})
				setTimeout(() => client.unsubscribe(this.topic), this.timeout)
			} catch (err) {
				console.error(err)
			}
	}
}

export default { Subscriber }