import { Subscriber } from './Subscriber'


const broker = 'mqtts://mqtt.hsl.fi:8883'
const vhs = ['bus', 'tram', 'metro', 'train']
for (let v in vhs) {
	const topic = `/hfp/v2/journey/ongoing/vp/${vhs[v]}/#`
	let verbosity = true
	let timeout = 0
	const subscriber = new Subscriber(broker, topic, vhs[v], verbosity, timeout)
	subscriber.subAndSave()
}

export default {}