import express, { Router } from 'express'
import bodyParser from 'body-parser'
import speedsRouter from './routes/speedsRouter'
import positionsRouter from './routes/positionsRouter'
import pool from './db/connectDB'


class Server {
	private app: express.Application

	constructor() {
		this.app = express();
		this.config();
		this.routerConfig();
		this.dbConnect();
	}

	private config() {
		this.app.use(bodyParser.json())
	}

	private routerConfig() {
		this.app.get('/speeds', speedsRouter)
		this.app.get('/positions', positionsRouter)
	}

	private async dbConnect() {
		try {
			await pool.connect()
			console.log('Connected to db')
		} catch (err) {
			console.error(err)
		}
	}

	public start = (port: number) => {
		return new Promise((resolve, reject) => {
			this.app.listen(port, () => {
				resolve(port)
			}).on('error', (err) => reject(err))
		})
	}
}

export default Server