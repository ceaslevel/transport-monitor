import express, { Router } from 'express'
import PositionsController from '../controllers/PositionsController'

const router = Router()
const positionsController = new PositionsController()

router.get('/positions', positionsController.get)

export default router