import express, { Router } from 'express'
import SpeedsController from '../controllers/SpeedsController'

const router = Router()
const speedsController = new SpeedsController()

router.get('/speeds', speedsController.get)

export default router