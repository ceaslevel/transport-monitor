import * as dotenv from 'dotenv'
import Server from './server'
import * as integrations from './integrations/index'
import * as db from './db/index'

dotenv.config({path: '/web/.env'})

const node_port = parseInt(process.env.NODE_PORT) || 3000
const server = new Server

server.start(node_port)
	.then(node_port => console.log(`Listening on ${node_port}`))
	.catch(error => console.log(error))

export default { server, integrations, db }