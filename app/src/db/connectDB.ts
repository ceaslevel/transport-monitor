import { Pool } from 'pg'
import * as dotenv from 'dotenv'

// env is not propagating well through the app
dotenv.config({path: '/web/.env'}) 

const pool: Pool = new Pool()

pool.on('error', (err) => {
	console.error('Pool error', err.message, err.stack)
})

export default pool