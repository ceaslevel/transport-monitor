import pool from './connectDB'


class InitDB {
	public async createPositionsTable() {

		const sql = `
		CREATE TABLE IF NOT EXISTS positions (

			id BIGSERIAL PRIMARY KEY,
			transport_mode VARCHAR(50),
			utime BIGINT,
			spd REAL,
			route VARCHAR(50),
			lat REAL,
			long REAL,
			hdg SMALLINT
		);`

		try {
			const client = await pool.connect()
			const start = Date.now()
			const result = await client.query(sql)
			const duration = Date.now() - start
			console.log('executed query', { sql, duration, rows: result.rowCount, result: result.rows })
			client.release()
		} catch (err) {
			console.error(err)
		}

	}
}

export default InitDB