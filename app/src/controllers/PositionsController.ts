import pool from '../db/connectDB'

interface Params {
	t: number | string
	lat: number | string
	long: number | string
}


class PositionsController {

	public async get(req, res) {
		const reqParams = req.query
		console.log('Params: ', reqParams)

		const params: Params = {
			t: new Date(reqParams.t).getTime() || `utime`,
			lat: +reqParams.lat || `lat`,
			long: +reqParams.transport_mode || `long`
		}

		const formula = `|/((utime-${params.t})^2+(lat-${params.lat})^2+(long-${params.long})^2)`
	
		let queryString = `
		WITH mindist AS (
			SELECT *, ${formula} AS dist 
			FROM positions 
			ORDER BY dist ASC LIMIT 1
		), alldist AS (
			SELECT *, ${formula} AS dist 
			FROM positions
		) 
		SELECT * 
		FROM alldist 
		WHERE dist 
		IN (
			SELECT dist FROM mindist
		)
		LIMIT 20;
		`

		console.log(queryString)

		try {
			const client = await pool.connect()
			// const sql = `SELECT NOW()`
			const start = Date.now()
			const result = await client.query(queryString)
			const duration = Date.now() - start
    		console.log('executed query', { queryString, duration, rows: result.rowCount })
			client.release()
			res.send(result.rows)
			}	catch (err) {
					res.status(400).send(err)
			}
	}
}

export default PositionsController