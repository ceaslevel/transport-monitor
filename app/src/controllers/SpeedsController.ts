import pool from '../db/connectDB'

interface Params {
	t1: number
	t2: number
	route?: string
	transport_mode?: string
}


class SpeedsController {

	public async get(req, res) {
		const reqParams = req.query
		console.log('Params: ', reqParams)

		const params: Params = {
			t1: new Date(reqParams.t1).getTime() || 0,
			t2: new Date(reqParams.t2).getTime() || 9223372036854775807,
			route: reqParams.route || null,
			transport_mode: reqParams.transport_mode || null
		}

		
		let queryString = `
		SELECT * FROM positions
		WHERE spd > 0 AND utime >= ${params.t1} AND utime <= ${params.t2}
		
		`
		if (params.route) queryString += ` AND route = '${String(params.route)}'`

		if (params.transport_mode) queryString += ` AND transport_mode = '${String(params.transport_mode)}'`

		queryString += `ORDER BY spd DESC LIMIT 20;`

		console.log(queryString)

		try {
			const client = await pool.connect()
			// const sql = `SELECT NOW()`
			const start = Date.now()
			const result = await client.query(queryString)
			const duration = Date.now() - start
    		console.log('executed query', { queryString, duration, rows: result.rowCount })
			client.release()
			res.send(result.rows)
			}	catch (err) {
					res.status(400).send(err)
			}
	}
}

export default SpeedsController