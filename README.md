# Transport Monitor

Retrieves positioning data from mqtt.hsl.fi of public transport vehicles.

## HSL Data

Topic subscribed to is `"/hfp/v2/journey/ongoing/vp/#"`.

Parameters of interest from topic:

* `transport_mode`: string [bus, tram, train, ferry, metro, ubus, robot]

Parameters of interest from message:

* `tst`: string with format `yyyy-MM-dd'T'HH:mm:ss.SSSZ` (iso timestamp)
* `spd`: float describing the speed of the vehicle in m/s
* `route`: string describing the route ID
* `lat`: float describing latitude (WGS84 degrees) or null
* `long`: float describing longitude (WGS84 degrees) or null
* `hdg`: integer on closed interval [0,360] i.e. (degrees)


## Endpoints

### Speeds

* Route: `/speeds` 
* Params:
    - `t1`: iso timestamp of start-time (do *not* wrap with quotes; inclusive)
    - `t2`: iso timestamp of end-time (do *not* wrap with quotes; inclusive)
    - `transport-mode`: string
    - `route-number`: string
* Returns:
    - List of vehicles with non-zero speed
    - ~~Each vehicle's highest speed during the time interval (vehicle route distinguishes vehicles as a proof of concept rather than unique tuples of operator and number)~~ Vehicles are listed in descending order of speed, and can be filtered by vehicle type and route name to find the respective highest speeds
    - Returns the 20 highest ranked items (note pagination is not implemented)

### Position

* Route: `/position`
* Params:
    - `lat`: latitude (float; not using correct format will result in interpretation of 'all latitudes')
    - `long`: longitude (float; not using correct format will result in interpretation of 'all longitudes')
    - `t`: iso timestamp of start-time (do wrap with quotes)
* Returns:
    - Vehicle closest in space and time
    - Return all closest vehicles if there is a tie
    - Returns limit is 20 (pagination is not implemented)

### Top Routes

> Not implemented.

* Route: `/top-routes`
* Params:
* Returns:
    - Route with most ever observed vehicles
    - Route with most cumulative degrees turned
    - Route with the highest average speed

## Running the application

For development:

```bash
cp .env.sample .env
make dev
```

Bootstrapped from  [docker-node](https://gitlab.com/ceaslevel/docker-node).

## Branching strategy

* `update/x` - update architecture, code refactoring, etc.
* `feature/x` - implements a feature
* `fix/x` - fixes something
* `document/x` - updates documentation, README, etc.