# see https://lithic.tech/blog/2020-05/makefile-dot-env/
ifneq (,$(wildcard ./.env))
    include .env
    export
endif

image_name := $(PROJECT_NAME)_$(NODE_SERVICE_NAME)
current_dir_host := $(shell pwd)
host_app_path := $(current_dir_host)/$(APP_NAME)
container_app_path := $(WEB_ROOT)/$(APP_NAME)

.PHONY: guard-% env_guard init dev deploy down

# see https://lithic.tech/blog/2020-05/makefile-wildcards/
guard-%:
	@if [ -z '${${*}}' ]; then echo 'ERROR: variable $* not set' && exit 1; fi

env_guard: guard-WEB_ROOT guard-APP_NAME guard-PROJECT_NAME \
 				guard-NODE_SERVICE_NAME guard-NODE_CONTAINER_NAME \
 				guard-DB_CONTAINER_NAME guard-PGDATABASE guard-PGUSER

# Sets up the project from scratch. Basically this creates
# a package.json and the project structure	
init: env_guard Dockerfile
	@printf 'Creating app directory\n***\n'
	mkdir $(APP_NAME) $(APP_NAME)/src

	@printf 'Adding yarn config: '
	@echo --modules-folder $(WEB_ROOT)/node_modules | \
		tee $(host_app_path)/.yarnrc

	@printf 'Building server image\n***\n'
	docker build . -t $(image_name)

	@printf 'Running container\n***\n'	
	docker run -it --rm \
		--mount 'type=bind,src=$(host_app_path),dst=$(container_app_path)' \
		--user node:node \
		--name $(NODE_CONTAINER_NAME) \
		--workdir $(container_app_path) \
		$(image_name) \
			/bin/sh -c \
			"yarn init -y && \
			yarn add -D typescript nodemon ts-node \
			@types/node @types/express && \
			yarn add express"
	
	@printf 'Initialised project\n***\n'

container: env_guard
	docker run -it --rm \
		--mount 'type=bind,src=$(host_app_path),dst=$(container_app_path)' \
		--user node:node \
		--name $(NODE_CONTAINER_NAME) \
		--workdir $(container_app_path) \
		$(image_name) \
			/bin/sh

cli: env_guard
	docker exec -it $(NODE_CONTAINER_NAME) /bin/sh

dbcli: env_guard
	docker exec -itu 0 $(DB_CONTAINER_NAME) /bin/bash -c "psql -d $(PGDATABASE) -U $(PGUSER) -W"

mqttcli:
	docker run -it hivemq/mqtt-cli shell

dev: env_guard docker-compose.yaml Dockerfile
	@printf 'Developing in container name: $(NODE_CONTAINER_NAME)\n***\n'
	docker-compose up

deploy: docker-compose.yaml Dockerfile

down: env_guard
	docker volume rm $(NODE_VOLUME_NAME)
	docker image rm $(image_name)

